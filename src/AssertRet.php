<?php namespace JCain\Asserts\LR;


/// Called by the provider in response to client-caused issues, so the client
/// can fix the issue.
class AssertRet {
	private function __construct() {
		// Do nothing.
	}


	static public function isNull($value, $name = '') {
		if ($value !== null)
			throw Exceptions::newUnexpectedValue($name, 'null', $value);
		return $value;
	}


	static public function isNotNull($value, $name = '') {
		if ($value === null)
			throw Exceptions::newUnexpectedValue($name, 'not null', $value);
		return $value;
	}


	static public function isBoolean($value, $name = '') {
		if ($value !== false && $value !== true)
			throw Exceptions::newUnexpectedValue($name, 'boolean', $value);
		return $value;
	}


	static public function isBooleanOrNull($value, $name = '') {
		if ($value !== null && $value !== false && $value !== true)
			throw Exceptions::newUnexpectedValue($name, 'boolean or null', $value);
		return $value;
	}


	static public function isNumber($value, $name = '') {
		$type = gettype($value)[0];
		if ($type !== 'i' && $type !== 'd')
			throw Exceptions::newUnexpectedValue($name, 'number', $value);
		return $value;
	}


	static public function isNumberOrNull($value, $name = '') {
		$type = gettype($value)[0];
		if ($type !== 'N' && $type !== 'i' && $type !== 'd')
			throw Exceptions::newUnexpectedValue($name, 'number or null', $value);
		return $value;
	}


	static public function isFloat($value, $name = '') {
		if (!is_float($value))
			throw Exceptions::newUnexpectedValue($name, 'float', $value);
		return $value;
	}


	static public function isFloatOrNull($value, $name = '') {
		if ($value !== null && !is_float($value))
			throw Exceptions::newUnexpectedValue($name, 'float or null', $value);
		return $value;
	}


	static public function isInteger($value, $name = '') {
		if (!is_int($value))
			throw Exceptions::newUnexpectedValue($name, 'integer', $value);
		return $value;
	}


	static public function isIntegerOrNull($value, $name = '') {
		if ($value !== null && !is_int($value))
			throw Exceptions::newUnexpectedValue($name, 'integer or null', $value);
		return $value;
	}


	static public function isString($value, $name = '') {
		if (!is_string($value))
			throw Exceptions::newUnexpectedValue($name, 'string', $value);
		return $value;
	}


	static public function isStringOrNull($value, $name = '') {
		if ($value !== null && !is_string($value))
			throw Exceptions::newUnexpectedValue($name, 'string or null', $value);
		return $value;
	}


	static public function isArray($value, $name = '') {
		if (!is_array($value))
			throw Exceptions::newUnexpectedValue($name, 'array', $value);
		return $value;
	}


	static public function isArrayOrNull($value, $name = '') {
		if ($value !== null && !is_array($value))
			throw Exceptions::newUnexpectedValue($name, 'array or null', $value);
		return $value;
	}


	static public function isArrayNonEmpty($value, $name = '') {
		if (!is_array($value) && !count($value))
			throw Exceptions::newUnexpectedValue($name, 'non-empty array', $value);
		return $value;
	}


	static public function isObject($value, $name = '') {
		if (!is_object($value))
			throw Exceptions::newUnexpectedValue($name, 'object', $value);
		return $value;
	}


	static public function isObjectOrNull($value, $name = '') {
		if ($value !== null && !is_object($value))
			throw Exceptions::newUnexpectedValue($name, 'object or null', $value);
		return $value;
	}


	static public function isResource($value, $name = '') {
		if (!is_resource($value))
			throw Exceptions::newUnexpectedValue($name, 'resource', $value);
		return $value;
	}


	static public function isResourceOrNull($value, $name = '') {
		if ($value !== null && !is_resource($value))
			throw Exceptions::newUnexpectedValue($name, 'resource or null', $value);
		return $value;
	}


	static public function isInstance($value, $class, $name = '') {
		if (!($value instanceof $class))
			throw Exceptions::newUnexpectedValue($name, "instance of $class", $value);
		return $value;
	}


	static public function isInstanceOrNull($value, $class, $name = '') {
		if ($value !== null && !($value instanceof $class))
			throw Exceptions::newUnexpectedValue($name, "instance of $class", $value);
		return $value;
	}


	static public function isClosure($value, $name = '') {
		return self::isInstance($value, 'Closure', $name);
	}


	static public function isClosureOrNull($value, $name = '') {
		return self::isInstanceOrNull($value, 'Closure', $name);
	}
}