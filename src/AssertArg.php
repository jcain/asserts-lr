<?php namespace JCain\Asserts\LR;


class AssertArg {
	private function __construct() {
		// Do nothing.
	}


	static public function isNull($value, $name = '') {
		if ($value !== null) {
			throw Exceptions::newInvalidArgument($name, 'null', $value);
		}
		return $value;
	}


	static public function isNotNull($value, $name = '') {
		if ($value === null) {
			throw Exceptions::newInvalidArgument($name, 'not null', $value);
		}
		return $value;
	}


	static public function isBoolean($value, $name = '') {
		if ($value !== false && $value !== true) {
			throw Exceptions::newInvalidArgument($name, 'boolean', $value);
		}
		return $value;
	}


	static public function isBooleanOrNull($value, $name = '') {
		if ($value !== null && $value !== false && $value !== true) {
			throw Exceptions::newInvalidArgument($name, 'boolean or null', $value);
		}
		return $value;
	}


	static public function isNumber($value, $name = '') {
		$type = gettype($value)[0];
		if ($type !== 'i' && $type !== 'd') {
			throw Exceptions::newInvalidArgument($name, 'number', $value);
		}
		return $value;
	}


	static public function isNumberOrNull($value, $name = '') {
		$type = gettype($value)[0];
		if ($type !== 'N' && $type !== 'i' && $type !== 'd') {
			throw Exceptions::newInvalidArgument($name, 'number or null', $value);
		}
		return $value;
	}


	static public function isFloat($value, $name = '') {
		if (!is_float($value)) {
			throw Exceptions::newInvalidArgument($name, 'float', $value);
		}
		return $value;
	}


	static public function isFloatOrNull($value, $name = '') {
		if ($value !== null && !is_float($value)) {
			throw Exceptions::newInvalidArgument($name, 'float or null', $value);
		}
		return $value;
	}


	static public function isInteger($value, $name = '') {
		if (!is_int($value) && (!is_float($value) || $value != (int)$value)) {
			throw Exceptions::newInvalidArgument($name, 'integer', $value);
		}
		return (int)$value;
	}


	static public function isIntegerOrNull($value, $name = '') {
		if ($value !== null && !is_int($value) && (!is_float($value) || $value != (int)$value)) {
			throw Exceptions::newInvalidArgument($name, 'integer or null', $value);
		}
		return ($value !== null ? (int)$value : null);
	}


	static public function isString($value, $name = '') {
		if (!is_string($value)) {
			throw Exceptions::newInvalidArgument($name, 'string', $value);
		}
		return $value;
	}


	static public function isStringOrNull($value, $name = '') {
		if ($value !== null && !is_string($value)) {
			throw Exceptions::newInvalidArgument($name, 'string or null', $value);
		}
		return $value;
	}


	static public function isArray($value, $name = '') {
		if (!is_array($value)) {
			throw Exceptions::newInvalidArgument($name, 'array', $value);
		}
		return $value;
	}


	static public function isArrayOrNull($value, $name = '') {
		if ($value !== null && !is_array($value)) {
			throw Exceptions::newInvalidArgument($name, 'array or null', $value);
		}
		return $value;
	}


	static public function isArrayNonEmpty($value, $name = '') {
		if (!is_array($value) && !count($value)) {
			throw Exceptions::newInvalidArgument($name, 'non-empty array', $value);
		}
		return $value;
	}


	static public function isObject($value, $name = '') {
		if (!is_object($value)) {
			throw Exceptions::newInvalidArgument($name, 'object', $value);
		}
		return $value;
	}


	static public function isObjectOrNull($value, $name = '') {
		if ($value !== null && !is_object($value)) {
			throw Exceptions::newInvalidArgument($name, 'object or null', $value);
		}
		return $value;
	}


	static public function isResource($value, $name = '') {
		if (!is_resource($value)) {
			throw Exceptions::newInvalidArgument($name, 'resource', $value);
		}
		return $value;
	}


	static public function isResourceOrNull($value, $name = '') {
		if ($value !== null && !is_resource($value)) {
			throw Exceptions::newInvalidArgument($name, 'resource or null', $value);
		}
		return $value;
	}


	static public function isInstance($value, $class, $name = '') {
		if (!($value instanceof $class)) {
			throw Exceptions::newInvalidArgument($name, "instance of $class", $value);
		}
		return $value;
	}


	static public function isInstanceOrNull($value, $class, $name = '') {
		if ($value !== null && !($value instanceof $class)) {
			throw Exceptions::newInvalidArgument($name, "instance of $class", $value);
		}
		return $value;
	}


	static public function isClosure($value, $name = '') {
		return self::isInstance($value, 'Closure', $name);
	}


	static public function isClosureOrNull($value, $name = '') {
		return self::isInstanceOrNull($value, 'Closure', $name);
	}
}