# JCain\Asserts\LR\Exceptions


## Exceptions class

_Stability: **alpha**, Since: **0.0**_

Factory and utility functions for various [`Exception`](https://www.php.net/manual/en/class.exception.php) subclasses.

```php
class Exceptions {
    // Constructor
    private function __construct();

    // Static Methods
    static public function newBadMethodCall(string $name);
    static public function newInvalidArgument($name, string $expected, $value);
    static public function newInvalidState();
    static public function newUnexpectedValue(string $expected, $value);
    static public function newNotImplemented();
    static public function newUnexpectedCondition();
    static public function typeOf($value);
}
```


### Constructor


#### __construct()

```php
private function __construct()
```


### Methods


#### newBadMethodCall()

```php
static public function newBadMethodCall(string $name)
```

Returns a [`BadMethodCallException`](https://www.php.net/manual/en/class.badmethodcallexception.php) to throw when the client tries to call a method that does not exist.

Intended for the client to fix the issue.


#### newInvalidArgument()

```php
static public function newInvalidArgument($name, string $expected, $value)
```

Returns an [`InvalidArgumentException`](https://www.php.net/manual/en/class.invalidargumentexception.php) to throw when the client calls a method with an argument value that is not allowed.

Intended for the client to fix the issue.


#### newInvalidState()

```php
static public function newInvalidState()
```

Returns an [`InvalidStateException`](InvalidStateException.api.md) to thrown when the client calls a method in the wrong order or with an invalid configuration.

Intended for the client to fix the issue.


#### newUnexpectedValue()

```php
static public function newUnexpectedValue(string $expected, $value)
```

Returns an [`UnexpectedValueException`](https://www.php.net/manual/en/class.unexpectedvalueexception.php) to throw when the client provides a callback and it returns a value that isn't what was expected.

Intended for the client to fix the issue.


#### newNotImplemented()

```php
static public function newNotImplemented()
```

Returns a [`NotImplementedException`](NotImplementedException.api.md) to throw when the client calls a method that has not yet been implemented by the provider.

Intended for the provider to fix the issue.


#### newUnexpectedCondition()

```php
static public function newUnexpectedCondition()
```

Returns a [`UnexpectedConditionException`](UnexpectedConditionException.api.md) to throw when the logic is faulty and lead to an unhandled condition.

Intended for the provider to fix the issue.

##### Example

```php
switch ($value) {
    case 'foo':
        // Good.
        break;

    case 'bar':
        // Nice.
        break;

    default:
        // Whoops! This condition should never happen.
        // $value should only be either 'foo' or 'bar'.
        throw Exceptions::newUnexpectedCondition();
}
```


#### typeOf()

```php
static public function typeOf($value)
```

Returns a string for `$value` for use in exception messages.