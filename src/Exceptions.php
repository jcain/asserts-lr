<?php namespace JCain\Asserts\LR;


class Exceptions {
	private function __construct() {
		// Do nothing.
	}


	static public function newBadMethodCall(string $name) {
		return new \BadMethodCallException("Call to undefined method $name()");
	}


	static public function newInvalidArgument($name, string $expected, $value) {
		return new \InvalidArgumentException(($name || $name === 0 || $name === 0.0 ? "$name : " : '') . "Expected $expected; got " . self::typeOf($value));
	}


	static public function newInvalidState() {
		return new InvalidStateException();
	}


	static public function newUnexpectedValue(string $expected, $value) {
		return new \UnexpectedValueException("Return expected $expected; got " . self::typeOf($value));
	}


	static public function newNotImplemented() {
		return new NotImplementedException();
	}


	static public function newUnexpectedCondition() {
		return new UnexpectedConditionException();
	}


	static public function typeOf($value) {
		$type = gettype($value);

		if ($type === 'NULL')
			return 'null';

		if ($type === 'double')
			return 'float';

		if ($type === 'object')
			return 'instance of ' . get_class($value);

		return $type;
	}
}