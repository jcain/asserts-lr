# JCain\Asserts\LR\UnexpectedConditionException


## UnexpectedConditionException class

_Stability: **alpha**, Since: **0.0**_

Thrown when the logic is faulty and lead to an unhandled condition.

```php
class UnexpectedConditionException extends LogicException {
    // No additional members.
}
```

Intended for the provider to fix the issue.


##### Example

```php
switch ($value) {
    case 'foo':
        // Good.
        break;

    case 'bar':
        // Nice.
        break;

    default:
        // Whoops! This condition should never happen.
        // $value should only be either 'foo' or 'bar'.
        throw Exceptions::newUnexpectedCondition();
}
```