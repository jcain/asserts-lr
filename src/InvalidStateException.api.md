# JCain\Asserts\LR\InvalidStateException


## InvalidStateException class

_Stability: **alpha**, Since: **0.0**_

Thrown when the client calls a method in the wrong order or with an invalid configuration.

```php
class InvalidStateException extends LogicException {
    // No additional members.
}
```

Intended for the client to fix the issue.