# JCain\Asserts\LR\NotImplementedException


## NotImplementedException class

_Stability: **alpha**, Since: **0.0**_

Thrown when the client calls a method that has not yet been implemented by the provider.

```php
class NotImplementedException extends LogicException {
    // No additional members.
}
```

Intended for the provider to fix the issue.