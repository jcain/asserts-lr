# JCain\Asserts\LR\AssertArg


## AssertArg class

_Stability: **alpha**, Since: **0.0**_

Functions for checking the arguments passed to a method or function.

```php
class AssertArg {
    // Constructor
    private function __construct()

    // Static Methods
    static public function isNull($value, $name = '')
    static public function isNotNull($value, $name = '')
    static public function isBoolean($value, $name = '')
    static public function isBooleanOrNull($value, $name = '')
    static public function isNumber($value, $name = '')
    static public function isNumberOrNull($value, $name = '')
    static public function isFloat($value, $name = '')
    static public function isFloatOrNull($value, $name = '')
    static public function isInteger($value, $name = '')
    static public function isIntegerOrNull($value, $name = '')
    static public function isString($value, $name = '')
    static public function isStringOrNull($value, $name = '')
    static public function isArray($value, $name = '')
    static public function isArrayOrNull($value, $name = '')
    static public function isArrayNonEmpty($value, $name = '')
    static public function isObject($value, $name = '')
    static public function isObjectOrNull($value, $name = '')
    static public function isResource($value, $name = '')
    static public function isResourceOrNull($value, $name = '')
    static public function isInstance($value, $class, $name = '')
    static public function isInstanceOrNull($value, $class, $name = '')
    static public function isClosure($value, $name = '')
    static public function isClosureOrNull($value, $name = '')
}
```


### Constructor


#### __construct()

```php
private function __construct()
```


### Methods


#### isNull()

```php
static public function isNull($value, $name = '')
```


#### isNotNull()

```php
static public function isNotNull($value, $name = '')
```


#### isBoolean()

```php
static public function isBoolean($value, $name = '')
```


#### isBooleanOrNull()

```php
static public function isBooleanOrNull($value, $name = '')
```


#### isNumber()

```php
static public function isNumber($value, $name = '')
```


#### isNumberOrNull()

```php
static public function isNumberOrNull($value, $name = '')
```


#### isFloat()

```php
static public function isFloat($value, $name = '')
```


#### isFloatOrNull()

```php
static public function isFloatOrNull($value, $name = '')
```


#### isInteger()

```php
static public function isInteger($value, $name = '')
```


#### isIntegerOrNull()

```php
static public function isIntegerOrNull($value, $name = '')
```


#### isString()

```php
static public function isString($value, $name = '')
```


#### isStringOrNull()

```php
static public function isStringOrNull($value, $name = '')
```


#### isArray()

```php
static public function isArray($value, $name = '')
```


#### isArrayOrNull()

```php
static public function isArrayOrNull($value, $name = '')
```


#### isArrayNonEmpty()

```php
static public function isArrayNonEmpty($value, $name = '')
```


#### isObject()

```php
static public function isObject($value, $name = '')
```


#### isObjectOrNull()

```php
static public function isObjectOrNull($value, $name = '')
```


#### isResource()

```php
static public function isResource($value, $name = '')
```


#### isResourceOrNull()

```php
static public function isResourceOrNull($value, $name = '')
```


#### isInstance()

```php
static public function isInstance($value, $class, $name = '')
```


#### isInstanceOrNull()

```php
static public function isInstanceOrNull($value, $class, $name = '')
```


#### isClosure()

```php
static public function isClosure($value, $name = '')
```


#### isClosureOrNull()

```php
static public function isClosureOrNull($value, $name = '')
```